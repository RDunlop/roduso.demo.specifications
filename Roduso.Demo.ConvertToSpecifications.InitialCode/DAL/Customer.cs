﻿namespace Roduso.Demo.ConvertToSpecifications.InitialCode.DAL
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}