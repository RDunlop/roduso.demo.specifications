﻿namespace Roduso.Demo.ConvertToSpecifications.InitialCode.DAL
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Amount { get; set; }
    }
}