﻿using System;
using System.Collections.Generic;

namespace Roduso.Demo.ConvertToSpecifications.InitialCode.DAL
{
    public class Context : IDisposable
    {
        public List<Order> Orders { get; private set; }
        public List<Article> Articles { get; private set; }
        public List<Customer> Customers { get; private set; }

        public void AddOrder(Order order)
        {            
            Orders.Add(order);
        }
            
        public int SaveChanges()
        {
            return 0;
        }

        public void Dispose()
        {            
        }
    }
}