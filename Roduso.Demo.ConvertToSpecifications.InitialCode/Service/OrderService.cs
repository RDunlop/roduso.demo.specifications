﻿using Roduso.Demo.ConvertToSpecifications.InitialCode.BLL;
using Roduso.Demo.ConvertToSpecifications.InitialCode.DTO;

namespace Roduso.Demo.ConvertToSpecifications.InitialCode.Service
{
    public static class OrderService
    {
        public static PlaceOrderResponseDto PlaceOrder(PlaceOrderRequestDto request)
        {
            return OrderHandler.PlaceOrder(request);
        }
    }
}