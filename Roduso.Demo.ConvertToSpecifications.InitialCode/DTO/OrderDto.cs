﻿using System;

namespace Roduso.Demo.ConvertToSpecifications.InitialCode.DTO
{
    public class OrderDto
    {
        public int Number { get; set; }
        public int? ArticleId { get; set; }
        public int NumberOfArticles { get; set; }
        public double Amount { get; set; }
        public int PlacedBy { get; set; }
        public DateTime PlacedAt { get; set; }
    }
}