﻿using System;
using System.Linq;
using Roduso.Demo.ConvertToSpecifications.InitialCode.DAL;
using Roduso.Demo.ConvertToSpecifications.InitialCode.DTO;

namespace Roduso.Demo.ConvertToSpecifications.InitialCode.BLL
{
    public class OrderHandler
    {
        public static PlaceOrderResponseDto PlaceOrder(PlaceOrderRequestDto request)
        {
            if (request.OrderDto == null)
            {
                throw new Exception("An order to place must be specified");
            }

            if (request.OrderDto.NumberOfArticles <= 0)
            {                
                throw new Exception("Number of articles must be a positive value greater than 0");
            }

            if (request.OrderDto.ArticleId == null)
            {
                throw new Exception("An article must be given");
            }

            if (request.OrderDto.Amount <= 0)
            {
                throw new Exception("The amount must be a positive value greater than 0");
            }

            using (var context = new Context())
            {
                int placedBy = GetCallingUserId();
                DateTime orderDate = DateTime.Now;

                Order order = new Order();
                order.Number = request.OrderDto.Number;
                order.ArticleId = request.OrderDto.ArticleId.Value;
                order.Amount = context.Articles.Single(x => x.Id == request.OrderDto.ArticleId).Amount * request.OrderDto.NumberOfArticles * 1.25;
                order.PlacedBy = placedBy;
                order.PlacedAt = orderDate;

                context.AddOrder(order);
                context.SaveChanges();

                OrderDto resultingOrderDto = new OrderDto();
                resultingOrderDto.Number = order.Number;
                resultingOrderDto.ArticleId = order.ArticleId;
                resultingOrderDto.NumberOfArticles = order.NumberOfArticles;
                resultingOrderDto.Amount = order.Amount;
                resultingOrderDto.PlacedBy = order.PlacedBy;
                resultingOrderDto.PlacedAt = order.PlacedAt;
                
                var customer = context.Customers.Single(x => x.Id == order.PlacedBy);
                string orderMessage = "An order was placed by " + customer.Name + " containing " + order.NumberOfArticles + " items of the article " + context.Articles.Single(x=>x.Id == order.ArticleId).Name;

                Communication communication = new Communication();
                communication.NotifyCustomer(customer.Email, orderMessage);

                PlaceOrderResponseDto response = new PlaceOrderResponseDto();
                response.OrderDto = resultingOrderDto;

                return response;
            }
        }

        private static int GetCallingUserId()
        {
            return 1;
        }
    }
}