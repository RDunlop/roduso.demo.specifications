﻿using System.Linq;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Data
{
    public class OrderNumberSeries : IOrderNumberSeries
    {
        public int GetNext()
        {
            using (var db = new Context())
            {
                var highestnumber = db.Orders.OrderByDescending(x => x.Number).SingleOrDefault()?.Number;
                return highestnumber ?? 1;
            }
        }
    }
}