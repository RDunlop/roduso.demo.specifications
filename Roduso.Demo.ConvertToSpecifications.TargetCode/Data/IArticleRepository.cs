﻿using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Data
{
    public interface IArticleRepository
    {
        Article LoadArticle(int articleId);
        void SaveArticle(Article article);
    }
}