﻿using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Data
{
    public interface IOrderRepository
    {
        Order LoadOrder(int orderNumber);
        void SaveOrder(Order order);
    }
}