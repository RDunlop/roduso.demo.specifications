﻿using System.Linq;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Data
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IContextFactory _contextFactory;

        public OrderRepository(IContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Order LoadOrder(int orderNumber)
        {
            using (var db = _contextFactory.CreateContext())
            {
                return db.Orders.SingleOrDefault(x => x.Number == orderNumber);
            }    
        }

        public void SaveOrder(Order order)
        {
            using (var db = _contextFactory.CreateContext())
            {
                if (db.Orders.Any(x => x.Number == order.Number) == false)
                {
                    db.AddOrder(order);
                }

                db.SaveChanges();
            }
        }
    }
}