﻿using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Data
{
    public interface ICustomerRepository
    {
        Customer LoadCustomer(int customerId);
        void SaveCustomer(Customer customer);
    }
}