﻿namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Data
{
    public interface IContextFactory
    {
        Context CreateContext();
    }
}