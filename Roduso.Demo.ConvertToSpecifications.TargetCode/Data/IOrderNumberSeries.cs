﻿namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Data
{
    public interface IOrderNumberSeries
    {
        int GetNext();
    }
}