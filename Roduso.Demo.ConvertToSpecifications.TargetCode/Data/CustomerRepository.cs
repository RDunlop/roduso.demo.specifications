﻿using System.Linq;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Data
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IContextFactory _contextFactory;

        public CustomerRepository(IContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Customer LoadCustomer(int customerId)
        {
            using (var db = _contextFactory.CreateContext())
            {
                return db.Customers.SingleOrDefault(x => x.Id == customerId);
            }    
        }

        public void SaveCustomer(Customer customer)
        {
            using (var db = _contextFactory.CreateContext())
            {
                if (db.Customers.Any(x => x.Id == customer.Id) == false)
                {
                    db.AddCustomer(customer);
                }

                db.SaveChanges();
            }
        }
    }
}