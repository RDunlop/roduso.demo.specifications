﻿using System.Linq;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Data
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly IContextFactory _contextFactory;

        public ArticleRepository(IContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        public Article LoadArticle(int articleId)
        {
            using (var db = _contextFactory.CreateContext())
            {
                return db.Articles.SingleOrDefault(x => x.Id == articleId);
            }
        }

        public void SaveArticle(Article article)
        {
            using (var db = _contextFactory.CreateContext())
            {
                if (db.Articles.Any(x => x.Id == article.Id) == false)
                {
                    db.AddArticle(article);
                }

                db.SaveChanges();
            }
        }
    }
}