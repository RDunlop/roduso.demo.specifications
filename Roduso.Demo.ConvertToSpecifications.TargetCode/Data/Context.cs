﻿using System;
using System.Collections.Generic;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Data
{
    public class Context : IDisposable
    {
        public List<Order> Orders { get; private set; }
        public List<Article> Articles { get; private set; }
        public List<Customer> Customers { get; private set; }

        public Context()
        {
            Orders = new List<Order>();
            Articles = new List<Article>();
            Customers = new List<Customer>();
        }

        public void AddOrder(Order order)
        {
            Orders.Add(order);
        }

        public void AddArticle(Article article)
        {
            Articles.Add(article);
        }

        public void AddCustomer(Customer customer)
        {
            Customers.Add(customer);
        }

        public int SaveChanges()
        {
            return 0;
        }

        public void Dispose()
        {
        }
    }
}