﻿namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Data
{
    public class ContextFactory : IContextFactory
    {
        public Context CreateContext()
        {
            return new Context();
        }
    }
}