namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Security
{
    public interface IPrincipal
    {
        int GetUserId();
    }
}