﻿using Roduso.Demo.ConvertToSpecifications.TargetCode.Data;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Security;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Communication
{
    public class Communication : ICommunication
    {
        private readonly IPrincipal _principal;
        private readonly ICustomerRepository _customerRepository;

        public Communication(IPrincipal principal, ICustomerRepository customerRepository)
        {
            _principal = principal;
            _customerRepository = customerRepository;
        }

        public void NotifyCustomer(string message)
        {
            var customer = _customerRepository.LoadCustomer(_principal.GetUserId());
            string email =  customer.Email;
        }
    }
}