﻿namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Communication
{
    public interface ICommunication
    {
        void NotifyCustomer(string message);
    }
}