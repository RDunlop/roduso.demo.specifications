﻿using Roduso.Demo.ConvertToSpecifications.TargetCode.Communication;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Data;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderPlacement
{
    public class PlaceOrderCommandHandler : IPlaceOrderCommandHandler
    {
        private readonly IOrderFactory _orderFactory;
        private readonly IOrderRepository _orderRepository;
        private readonly ICommunication _communication;
        private readonly IOrderPlacedNotificationBuilder _orderPlacedNotificationBuilder;

        public PlaceOrderCommandHandler(
            IOrderFactory orderFactory,
            IOrderRepository orderRepository,
            ICommunication communication,
            IOrderPlacedNotificationBuilder orderPlacedNotificationBuilder)
        {
            _orderFactory = orderFactory;
            _orderRepository = orderRepository;
            _communication = communication;
            _orderPlacedNotificationBuilder = orderPlacedNotificationBuilder;
        }

        public void PlaceOrder(PlaceOrderCommand placeOrderCommand)
        {            
            var newOrder = _orderFactory.CreateOrder(placeOrderCommand);
            _orderRepository.SaveOrder(newOrder);
            NotifyCustomer(newOrder);
        }

        private void NotifyCustomer(Order newOrder)
        {
            var constructOrderPlacedNotificationMessage = _orderPlacedNotificationBuilder.ConstructOrderPlacedNotificationMessage(order: newOrder);
            _communication.NotifyCustomer(constructOrderPlacedNotificationMessage);
        }
    }
}