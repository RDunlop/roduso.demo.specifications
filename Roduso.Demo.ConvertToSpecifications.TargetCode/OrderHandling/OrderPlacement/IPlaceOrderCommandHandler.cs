﻿namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderPlacement
{
    public interface IPlaceOrderCommandHandler
    {
        void PlaceOrder(PlaceOrderCommand placeOrderCommand);
    }
}