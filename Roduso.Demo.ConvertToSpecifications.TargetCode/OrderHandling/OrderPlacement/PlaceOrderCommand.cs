﻿namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderPlacement
{
    public class PlaceOrderCommand
    {
        public int ArticleId { get; set; }
        public int NumberOfArticles { get; set; }
    }
}