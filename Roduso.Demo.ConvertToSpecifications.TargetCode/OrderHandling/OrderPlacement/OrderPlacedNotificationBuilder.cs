﻿using Roduso.Demo.ConvertToSpecifications.TargetCode.Data;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Security;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderPlacement
{
    public class OrderPlacedNotificationBuilder : IOrderPlacedNotificationBuilder
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IArticleRepository _articleRepository;
        private readonly IPrincipal _principal;

        public OrderPlacedNotificationBuilder(
            ICustomerRepository customerRepository, 
            IArticleRepository articleRepository,
            IPrincipal principal)
        {
            _customerRepository = customerRepository;
            _articleRepository = articleRepository;
            _principal = principal;
        }

        public string ConstructOrderPlacedNotificationMessage(Order order)
        {
            var userId = _principal.GetUserId();

            var customer  = _customerRepository.LoadCustomer(userId);
            return "An order was placed by " + customer.Name + " containing " + order.NumberOfArticles + " items of the article " + _articleRepository.LoadArticle(order.ArticleId).Name;
        }
    }
}