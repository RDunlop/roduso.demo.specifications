﻿using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderPlacement
{
    public interface IOrderFactory
    {
        Order CreateOrder(PlaceOrderCommand placeOrderCommand);
    }
}