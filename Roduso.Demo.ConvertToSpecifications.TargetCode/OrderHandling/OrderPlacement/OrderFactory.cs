﻿using Roduso.Demo.ConvertToSpecifications.TargetCode.Data;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Security;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderPlacement
{
    public class OrderFactory : IOrderFactory
    {
        private readonly IOrderNumberSeries _orderNumberSeries;
        private readonly IArticleRepository _articleRepository;
        private readonly IPrincipal _principal;

        public OrderFactory(
            IOrderNumberSeries orderNumberSeries,
            IArticleRepository articleRepository,
            IPrincipal principal)
        {
            _orderNumberSeries = orderNumberSeries;
            _articleRepository = articleRepository;
            _principal = principal;
        }

        public Order CreateOrder(PlaceOrderCommand placeOrderCommand)
        {
            var userId = _principal.GetUserId();
            var article = _articleRepository.LoadArticle(placeOrderCommand.ArticleId);

            Order newOrder =
                new Order(
                    orderNumberSeries: _orderNumberSeries,
                    articleId: placeOrderCommand.ArticleId,
                    numberOfArticles: placeOrderCommand.NumberOfArticles,
                    articleAmount: article?.Amount ?? -1,
                    placedBy: userId);
            return newOrder;
        }
    }
}