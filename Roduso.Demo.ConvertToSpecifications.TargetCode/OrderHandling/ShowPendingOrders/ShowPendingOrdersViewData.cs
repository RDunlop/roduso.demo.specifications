using System.Collections.Generic;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.ShowPendingOrders
{
    public class ShowPendingOrdersViewData
    {
        public List<PendingOrderViewData> PendingOrders { get; set; }
    }
}