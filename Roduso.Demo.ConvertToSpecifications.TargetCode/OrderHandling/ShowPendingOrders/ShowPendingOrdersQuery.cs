﻿using System.Linq;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Data;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.ShowPendingOrders
{
    public class ShowPendingOrdersQuery
    {
        public ShowPendingOrdersViewData Execute()
        {
            using (var db = new Context())
            {
                var pendingOrders = 
                    db
                    .Orders
                    .Where(o => o.IsShipped == false)
                    .Select(pendingOrder => new PendingOrderViewData
                    {
                        OrderNumber = pendingOrder.Number,
                        ArticleName = db.Articles.Single(a => a.Id == pendingOrder.ArticleId).Name,
                        NumberOfArticles = pendingOrder.NumberOfArticles,
                        TotalAmount = pendingOrder.TotalAmount
                    })
                    .ToList();

                return new ShowPendingOrdersViewData
                {
                    PendingOrders = pendingOrders                    
                };
            }
        }
    }
}