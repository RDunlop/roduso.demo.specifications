﻿namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderSuspending
{
    public class SuspendOrderCommand
    {
        public int OrderNumber { get; set; }
    }
}