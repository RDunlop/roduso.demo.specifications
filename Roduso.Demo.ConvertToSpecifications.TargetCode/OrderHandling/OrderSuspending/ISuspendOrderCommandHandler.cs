namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderSuspending
{
    public interface ISuspendOrderCommandHandler
    {
        void SuspendOrder(SuspendOrderCommand suspendOrderCommand);
    }
}