﻿using Roduso.Demo.ConvertToSpecifications.TargetCode.Data;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderShipping
{
    public class ShipOrderCommandHandler : IShipOrderCommandHandler
    {
        private readonly IOrderRepository _orderRepository;

        public ShipOrderCommandHandler(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public void ShipOrder(ShipOrderCommand shipOrderCommand)
        {
            var order = _orderRepository.LoadOrder(shipOrderCommand.OrderNumber);

            order.ShipOrder();

            _orderRepository.SaveOrder(order);
        }
    }
}