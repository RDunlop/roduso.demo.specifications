﻿namespace Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderShipping
{
    public interface IShipOrderCommandHandler
    {
        void ShipOrder(ShipOrderCommand shipOrderCommand);
    }
}