﻿namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Model
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
    }
}