﻿using System;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Data;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Model
{
    public class Order
    {
        public int Number { get; private set; }
        public int ArticleId { get; private set; }
        public int NumberOfArticles { get; private set; }
        public double TotalAmount { get; private set; }
        public int PlacedBy { get; private set; }
        public DateTime PlacedAt { get; private set; }
        public bool IsShipped { get; private set; }
        public bool IsSuspended { get; private set; }

        public Order(IOrderNumberSeries orderNumberSeries, int articleId, int numberOfArticles, double articleAmount, int placedBy)
        {
            ValidateArticle(articleId, numberOfArticles, articleAmount);

            Number = orderNumberSeries.GetNext();
            ArticleId = articleId;
            TotalAmount = numberOfArticles * articleAmount;
            PlacedBy = placedBy;
            PlacedAt = DateTime.Now;
        }

        public void ShipOrder()
        {
            if (IsSuspended)
            {
                throw new Exception("The order cannot be shipped since it is previously suspended");
            }

            IsShipped = true;
        }

        public void SuspendOrder()
        {
            if (IsShipped)
            {
                throw new Exception("The order cannot be suspended since it is previously shipped");
            }

            IsSuspended = true;
        }

        private void ValidateArticle(int articleId, int numberOfArticles, double articleAmount)
        {
            if (articleId <= 0)
            {
                throw new ArgumentException("An article must be given");
            }

            if (numberOfArticles <= 0)
            {
                throw new ArgumentException("Number of articles must be a positive value greater than 0");
            }

            if (articleAmount < 0)
            {
                throw new ArgumentException("The amount must be a positive value or equal to 0");
            }
        }
    }
}