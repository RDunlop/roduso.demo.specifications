﻿using Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderPlacement;
using Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderShipping;
using Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderSuspending;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Services
{
    public class OrderService
    {
        public OrderService(
            IPlaceOrderCommandHandler placeOrderCommandHandler,
            IShipOrderCommandHandler shipOrderCommandHandler)
        {
            
        }

        public void PlaceOrder(PlaceOrderCommand placeOrderCommand)
        {
            
        }

        public void ShipOrder(ShipOrderCommand shipOrderCommand)
        {
            
        }

        public void SuspendOrder(SuspendOrderCommand suspendOrderCommand)
        {
            
        }
    }
}