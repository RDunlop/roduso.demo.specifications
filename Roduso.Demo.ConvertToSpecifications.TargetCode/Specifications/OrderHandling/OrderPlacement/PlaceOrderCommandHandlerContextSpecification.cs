using Moq;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Communication;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Data;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;
using Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderPlacement;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Specifications.OrderHandling.OrderPlacement
{
    public abstract class PlaceOrderCommandHandlerContextSpecification : ContextSpecification
    {
        protected IPlaceOrderCommandHandler PlaceOrderCommandHandler { get; set; }
        
        protected Mock<IOrderFactory> OrderFactoryMock { get; set; }
        protected Mock<IOrderRepository> OrderRepositoryMock { get; set; }
        protected Mock<ICommunication> CommunicationMock { get; set; }
        protected Mock<IOrderPlacedNotificationBuilder> OrderPlacedNotificationBuilderMock { get; set; }
        protected Mock<IOrderNumberSeries> OrderNumberSeriesMock { get; set; }        

        protected PlaceOrderCommand PlaceOrderCommand { get; set; }

        protected override void CreateMocks()
        {
            OrderFactoryMock = new Mock<IOrderFactory>(MockBehavior.Strict);
            OrderRepositoryMock = new Mock<IOrderRepository>(MockBehavior.Strict);
            CommunicationMock = new Mock<ICommunication>(MockBehavior.Strict);
            OrderPlacedNotificationBuilderMock = new Mock<IOrderPlacedNotificationBuilder>(MockBehavior.Strict);
            OrderNumberSeriesMock = new Mock<IOrderNumberSeries>(MockBehavior.Strict);
        }

        protected PlaceOrderCommandHandler CreateCommandHandler()
        {
            return new PlaceOrderCommandHandler(
                OrderFactoryMock.Object,
                OrderRepositoryMock.Object,
                CommunicationMock.Object,
                OrderPlacedNotificationBuilderMock.Object);
        }

        protected void GivenAnOrderIsPlaced()
        {
            PlaceOrderCommand = new PlaceOrderCommand();
        }

        protected void SimulateOrderCreation()
        {
            OrderNumberSeriesMock.Setup(x => x.GetNext()).Returns(3);
            var newOrder = new Order(OrderNumberSeriesMock.Object, articleId: 123, numberOfArticles: 2, articleAmount: 2.3, placedBy: 222);
            OrderFactoryMock.Setup(x => x.CreateOrder(It.IsAny<PlaceOrderCommand>())).Returns(newOrder);            
        }

        protected void SimulateSavingAnOrder()
        {
            OrderRepositoryMock.Setup(x => x.SaveOrder(It.IsAny<Order>())).Verifiable();
        }

        protected void SimulateNotifyingTheCustomer()
        {
            OrderPlacedNotificationBuilderMock.Setup(x => x.ConstructOrderPlacedNotificationMessage(It.IsAny<Order>())).Returns("Any old text");
            CommunicationMock.Setup(x => x.NotifyCustomer(It.IsAny<string>())).Verifiable();
        }
    }
}