﻿using Moq;
using NUnit.Framework;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;
using Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderPlacement;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Specifications.OrderHandling.OrderPlacement
{
    public class WhenAnOrderIsPlaced : PlaceOrderCommandHandlerContextSpecification
    {
        [Test]
        public void AnOrderShouldBeCreated()
        {
            OrderFactoryMock.Verify(x=>x.CreateOrder(It.IsAny<PlaceOrderCommand>()), Times.Once);
        }

        [Test]
        public void TheOrderShouldBePersisted()
        {
            OrderRepositoryMock.Verify(x=>x.SaveOrder(It.IsAny<Order>()), Times.Once);
        }

        [Test]
        public void TheCustomerShouldBeNotified()
        {
            OrderPlacedNotificationBuilderMock.Verify(x=>x.ConstructOrderPlacedNotificationMessage(It.IsAny<Order>()), Times.Once);
            CommunicationMock.Verify(x=>x.NotifyCustomer(It.IsAny<string>()), Times.Once);
        }

        protected override void Givens()
        {
            GivenAnOrderIsPlaced();
        }

        protected override void SetupMockBehaviour()
        {
            SimulateOrderCreation();
            SimulateSavingAnOrder();
            SimulateNotifyingTheCustomer();
        }

        protected override void InitializeStateOnTestClass()
        {
            PlaceOrderCommandHandler = CreateCommandHandler();
        }

        protected override void When()
        {
            PlaceOrderCommandHandler.PlaceOrder(PlaceOrderCommand);
        }
    }
}