using Moq;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Data;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;
using Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderPlacement;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Security;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Specifications.OrderHandling.OrderPlacement.OrderCreation
{
    public abstract class OrderFactoryContextSpecification : ContextSpecification
    {        
        protected IOrderFactory OrderFactory { get; set; }

        protected Mock<IOrderNumberSeries> OrderNumberSeriesMock { get; set; }
        protected Mock<IArticleRepository> ArticleRepositoryMock { get; set; }
        protected Mock<IPrincipal> PrincipalMock { get; set; }
        protected PlaceOrderCommand PlaceOrderCommand { get; set; }
        protected Article Article { get; set; }

        protected override void CreateMocks()
        {
            OrderNumberSeriesMock = new Mock<IOrderNumberSeries>(MockBehavior.Strict);
            ArticleRepositoryMock = new Mock<IArticleRepository>(MockBehavior.Strict);
            PrincipalMock = new Mock<IPrincipal>(MockBehavior.Strict);
        }

        protected void SimulateGenerationOfANewOrderNumber()
        {
            OrderNumberSeriesMock.Setup(x => x.GetNext()).Returns(4);
        }

        protected void SimulateLoadingAnArticle()
        {
            ArticleRepositoryMock.Setup(x => x.LoadArticle(It.IsAny<int>())).Returns(Article);
        }

        protected void SimulateRetreivingTheCallingUser()
        {
            PrincipalMock.Setup(x => x.GetUserId()).Returns(223);
        }

        protected void GivenAUserPlacesAnOrder(Article article, int numberOfArticles = 0)
        {
            PlaceOrderCommand = new PlaceOrderCommand { ArticleId = article?.Id ?? 0, NumberOfArticles = numberOfArticles };
        }

        protected Article GivenAnArticle(int id = 1, double amount = 0, string name = "Any old name")
        {
            Article = new Article { Id = id, Amount = amount, Name = name };
            return Article;
        }
    }
}