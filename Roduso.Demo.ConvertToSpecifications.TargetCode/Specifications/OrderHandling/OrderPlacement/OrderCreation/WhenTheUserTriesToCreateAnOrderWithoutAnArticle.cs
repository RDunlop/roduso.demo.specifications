﻿using System;
using NUnit.Framework;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;
using Roduso.Demo.ConvertToSpecifications.TargetCode.OrderHandling.OrderPlacement;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Specifications.OrderHandling.OrderPlacement.OrderCreation
{
    public class WhenTheUserTriesToCreateAnOrderWithoutAnArticle : OrderFactoryContextSpecification
    {
        private Order _newOrder;

        [Test]
        public void AnExceptionShouldBeThrown()
        {
            Assert.That(ActualException, Is.TypeOf(ExpectedExceptionType));
        }

        [Test]
        public void NoNewOrderShouldBeCreated()
        {
            Assert.That(_newOrder, Is.Null);
        }

        [Test]
        public void TheActualExceptionShouldStateThatAnArticleIsMissing()
        {
            Assert.That(ActualException.Message, Is.EqualTo("An article must be given"));
        }

        protected override void Givens()
        {
            GivenAUserPlacesAnOrder(article: null, numberOfArticles: 1);
        }

        public override Type ExpectedExceptionType => typeof(ArgumentException);

        protected override void SetupMockBehaviour()
        {
            SimulateGenerationOfANewOrderNumber();
            SimulateLoadingAnArticle();
            SimulateRetreivingTheCallingUser();
        }

        protected override void InitializeStateOnTestClass()
        {
            OrderFactory = new OrderFactory(OrderNumberSeriesMock.Object, ArticleRepositoryMock.Object, PrincipalMock.Object);
        }

        protected override void When()
        {
            _newOrder = OrderFactory.CreateOrder(PlaceOrderCommand);
        }
    }
}