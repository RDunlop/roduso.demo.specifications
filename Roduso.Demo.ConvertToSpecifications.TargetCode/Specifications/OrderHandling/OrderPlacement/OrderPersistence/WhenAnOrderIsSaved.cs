﻿using NUnit.Framework;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Data;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Specifications.OrderHandling.OrderPlacement.OrderPersistence
{
    public class WhenAnOrderIsSaved : OrderRepositoryContextSpecification
    {
        [Test]
        public void ItShouldBePossibleToLoadTheSavedOrder()
        {
            var loadedOrder = OrderRepository.LoadOrder(OrderNumber);
            Assert.That(loadedOrder, Is.EqualTo(Order));
        }

        protected override void Givens()
        {
            GivenAContext();
            GivenAnOrder();
        }

        protected override void SetupMockBehaviour()
        {
            SimulateTheDatabase();
        }

        protected override void InitializeStateOnTestClass()
        {
            OrderRepository = new OrderRepository(ContextFactoryMock.Object);
        }

        protected override void When()
        {
            OrderRepository.SaveOrder(Order);
        }
    }
}