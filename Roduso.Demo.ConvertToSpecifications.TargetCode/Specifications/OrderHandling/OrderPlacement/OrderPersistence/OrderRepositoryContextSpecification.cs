﻿using Moq;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Data;
using Roduso.Demo.ConvertToSpecifications.TargetCode.Model;

namespace Roduso.Demo.ConvertToSpecifications.TargetCode.Specifications.OrderHandling.OrderPlacement.OrderPersistence
{
    public abstract class OrderRepositoryContextSpecification : ContextSpecification
    {        
        protected IOrderRepository OrderRepository { get; set; }

        protected Mock<IContextFactory> ContextFactoryMock { get; set; }
        protected Mock<IOrderNumberSeries> OrderNumberSeriesMock { get; set; }

        protected Context Context { get; set; }
        protected Order Order { get; set; }
        protected int OrderNumber { get; set; }

        protected override void CreateMocks()
        {
            ContextFactoryMock = new Mock<IContextFactory>(MockBehavior.Strict);                        
            OrderNumberSeriesMock = new Mock<IOrderNumberSeries>(MockBehavior.Strict);            
        }

        protected void GivenAContext()
        {
            Context = new Context();
        }

        protected void GivenAnOrder()
        {
            SimulateTheNumberSeries();
            Order = new Order(OrderNumberSeriesMock.Object, 1, 1, 1, 1);
        }

        protected void SimulateTheNumberSeries()
        {
            OrderNumber = 234432;
            OrderNumberSeriesMock.Setup(x => x.GetNext()).Returns(OrderNumber);
        }

        protected void SimulateTheDatabase()
        {
            ContextFactoryMock.Setup(x => x.CreateContext()).Returns(Context);
        }
    }
}